import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

//My personal imports.
import '/providers/user_provider.dart';
import '/screens/login_screen.dart';
import '/screens/register_screen.dart';
import '/screens/task_list_screen.dart';
Future<void> main() async {
    await dotenv.load(fileName: "assets/env/.env_web");

    //Initial checks for user's access token from SharedPreferences.
    // Determines initial route of app depending on existence of user's access token.

    //This makes sure that everything is ready before executing the successding codes. 
    WidgetsFlutterBinding.ensureInitialized();

    //Responsible for reading and retrieving info from the device storage:
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? accessToken = prefs.getString('accessToken');
    String initialRoute = (accessToken != null ? '/task-list' : '/');

    runApp(App(accessToken, initialRoute));
}

class App extends StatelessWidget {
    final String? _accessToken;
    final String _initialRoute;

    App(this._accessToken, this._initialRoute);

    @override
    Widget build(BuildContext context) {
        return ChangeNotifierProvider(
            create: (BuildContext context)=>UserProvider(_accessToken),
            child: MaterialApp(
                theme: ThemeData(
                    primaryColor: Color.fromRGBO(255, 212, 71, 1),
                    elevatedButtonTheme: ElevatedButtonThemeData(
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(255,212, 71, 1), //Background color
                            onPrimary: Colors.black //Text Color
                        )
                    )
                ),
                initialRoute : _initialRoute,
                routes:{
                    '/': (context) => LoginScreen(),
                    '/register' : (context) => RegisterScreen(),
                    '/task-list' : (context) => TaskListScreen()
                }
            )
        );
    }
}